
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para levantarSancionMovil complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="levantarSancionMovil"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMovil" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "levantarSancionMovil", propOrder = {
    "idMovil",
    "disponible"
})
public class LevantarSancionMovil {

    protected int idMovil;
    protected boolean disponible;

    /**
     * Obtiene el valor de la propiedad idMovil.
     * 
     */
    public int getIdMovil() {
        return idMovil;
    }

    /**
     * Define el valor de la propiedad idMovil.
     * 
     */
    public void setIdMovil(int value) {
        this.idMovil = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible.
     * 
     */
    public boolean isDisponible() {
        return disponible;
    }

    /**
     * Define el valor de la propiedad disponible.
     * 
     */
    public void setDisponible(boolean value) {
        this.disponible = value;
    }

}
