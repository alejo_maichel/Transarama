
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para analizarTramaUnidades complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="analizarTramaUnidades"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="datosServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "analizarTramaUnidades", propOrder = {
    "datosServicio"
})
public class AnalizarTramaUnidades {

    protected String datosServicio;

    /**
     * Obtiene el valor de la propiedad datosServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatosServicio() {
        return datosServicio;
    }

    /**
     * Define el valor de la propiedad datosServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatosServicio(String value) {
        this.datosServicio = value;
    }

}
