
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asociarConductorMoviles complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asociarConductorMoviles"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="datosOperativa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asociarConductorMoviles", propOrder = {
    "datosOperativa"
})
public class AsociarConductorMoviles {

    protected String datosOperativa;

    /**
     * Obtiene el valor de la propiedad datosOperativa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatosOperativa() {
        return datosOperativa;
    }

    /**
     * Define el valor de la propiedad datosOperativa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatosOperativa(String value) {
        this.datosOperativa = value;
    }

}
