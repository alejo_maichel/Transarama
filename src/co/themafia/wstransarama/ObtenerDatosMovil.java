
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para obtenerDatosMovil complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="obtenerDatosMovil"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="porPlaca" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="tipoProyecto" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenerDatosMovil", propOrder = {
    "dato",
    "porPlaca",
    "tipoProyecto"
})
public class ObtenerDatosMovil {

    protected String dato;
    protected boolean porPlaca;
    protected int tipoProyecto;

    /**
     * Obtiene el valor de la propiedad dato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDato() {
        return dato;
    }

    /**
     * Define el valor de la propiedad dato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDato(String value) {
        this.dato = value;
    }

    /**
     * Obtiene el valor de la propiedad porPlaca.
     * 
     */
    public boolean isPorPlaca() {
        return porPlaca;
    }

    /**
     * Define el valor de la propiedad porPlaca.
     * 
     */
    public void setPorPlaca(boolean value) {
        this.porPlaca = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProyecto.
     * 
     */
    public int getTipoProyecto() {
        return tipoProyecto;
    }

    /**
     * Define el valor de la propiedad tipoProyecto.
     * 
     */
    public void setTipoProyecto(int value) {
        this.tipoProyecto = value;
    }

}
