package co.themafia.wstransarama.clientsample;

import co.themafia.wstransarama.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        WSFuncionesSistemaAsignacion_Service service1 = new WSFuncionesSistemaAsignacion_Service();
	        System.out.println("Create Web Service...");
	        WSFuncionesSistemaAsignacion port1 = service1.getWSFuncionesSistemaAsignacionPort();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.crearServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.actualizarDireccionUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.asociarConductorMoviles(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.geoReferenciarRuteoGeneral(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerListaServiciosMemoria(Integer.parseInt(args[0])));
	        System.out.println("Server said: " + port1.webLogInUsuarioSistema(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.eliminarRegistroProcesoServiciosAvianca(Integer.parseInt(args[1])));
	        System.out.println("Server said: " + port1.eliminarDireccionUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerListaMovilesConductoresWS(Integer.parseInt(args[2])));
	        System.out.println("Server said: " + port1.obtenerUsuarioSistema(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.actualizarParametro(null,null,Integer.parseInt(args[3])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.recibirDatosAvianca(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.cancelarServicioCentral(Integer.parseInt(args[4])));
	        System.out.println("Server said: " + port1.agregarModificarConductor(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.analizarTramaUnidades(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerListaServiciosHistoricosCompleta(Integer.parseInt(args[5])));
	        System.out.println("Server said: " + port1.obtenerListaTipoServicio(Integer.parseInt(args[6])));
	        System.out.println("Server said: " + port1.mensajesGrupales(null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.modificarTiempoMinimoYsancion(Integer.parseInt(args[7]),Integer.parseInt(args[8]),Integer.parseInt(args[9]),Integer.parseInt(args[10])));
	        System.out.println("Server said: " + port1.consultarDatosUsuario(null,Integer.parseInt(args[11])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.listarServiciosMotiva());
	        System.out.println("Server said: " + port1.consultarServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerListaServiciosHistoricosFecha(Integer.parseInt(args[12]),null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.listaServiciosAsignarAvianca());
	        System.out.println("Server said: " + port1.levantarSancionMovil(Integer.parseInt(args[13]),Boolean.parseBoolean(args[14])));
	        System.out.println("Server said: " + port1.enviarTramaUnidad(null,Integer.parseInt(args[15])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.crearServicioAvianca(null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.recibirCargaTripulacionAvianca(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.crearServicioDatosServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.agregarModificarMovil(null,Integer.parseInt(args[16])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.funcionesOperativa(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.verificarVersionActual(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.levantarSancionConductor(null,Boolean.parseBoolean(args[17]),Integer.parseInt(args[18])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.mensajesAlMovil(null,null,Integer.parseInt(args[19])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.actualizarModificarUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerDatosMovil(null,Boolean.parseBoolean(args[20]),Integer.parseInt(args[21])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: port1.imprimirServiciosAvtivos() is a void method!");
	        System.out.println("Server said: " + port1.obtenerDatosConductor(null,Boolean.parseBoolean(args[22]),Integer.parseInt(args[23])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.obtenerServicioXtel(null,Boolean.parseBoolean(args[24])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.listaUsuariosFinales(Integer.parseInt(args[25])));
	        System.out.println("Server said: " + port1.obtenerListaServiciosCompleta());
	        System.out.println("Create Web Service...");
	        WSFuncionesSistemaAsignacion port2 = service1.getWSFuncionesSistemaAsignacionPort();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.crearServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.actualizarDireccionUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.asociarConductorMoviles(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.geoReferenciarRuteoGeneral(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerListaServiciosMemoria(Integer.parseInt(args[26])));
	        System.out.println("Server said: " + port2.webLogInUsuarioSistema(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.eliminarRegistroProcesoServiciosAvianca(Integer.parseInt(args[27])));
	        System.out.println("Server said: " + port2.eliminarDireccionUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerListaMovilesConductoresWS(Integer.parseInt(args[28])));
	        System.out.println("Server said: " + port2.obtenerUsuarioSistema(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.actualizarParametro(null,null,Integer.parseInt(args[29])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.recibirDatosAvianca(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.cancelarServicioCentral(Integer.parseInt(args[30])));
	        System.out.println("Server said: " + port2.agregarModificarConductor(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.analizarTramaUnidades(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerListaServiciosHistoricosCompleta(Integer.parseInt(args[31])));
	        System.out.println("Server said: " + port2.obtenerListaTipoServicio(Integer.parseInt(args[32])));
	        System.out.println("Server said: " + port2.mensajesGrupales(null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.modificarTiempoMinimoYsancion(Integer.parseInt(args[33]),Integer.parseInt(args[34]),Integer.parseInt(args[35]),Integer.parseInt(args[36])));
	        System.out.println("Server said: " + port2.consultarDatosUsuario(null,Integer.parseInt(args[37])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.listarServiciosMotiva());
	        System.out.println("Server said: " + port2.consultarServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerListaServiciosHistoricosFecha(Integer.parseInt(args[38]),null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.listaServiciosAsignarAvianca());
	        System.out.println("Server said: " + port2.levantarSancionMovil(Integer.parseInt(args[39]),Boolean.parseBoolean(args[40])));
	        System.out.println("Server said: " + port2.enviarTramaUnidad(null,Integer.parseInt(args[41])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.crearServicioAvianca(null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.recibirCargaTripulacionAvianca(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.crearServicioDatosServicio(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.agregarModificarMovil(null,Integer.parseInt(args[42])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.funcionesOperativa(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.verificarVersionActual(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.levantarSancionConductor(null,Boolean.parseBoolean(args[43]),Integer.parseInt(args[44])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.mensajesAlMovil(null,null,Integer.parseInt(args[45])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.actualizarModificarUsuario(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerDatosMovil(null,Boolean.parseBoolean(args[46]),Integer.parseInt(args[47])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: port2.imprimirServiciosAvtivos() is a void method!");
	        System.out.println("Server said: " + port2.obtenerDatosConductor(null,Boolean.parseBoolean(args[48]),Integer.parseInt(args[49])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.obtenerServicioXtel(null,Boolean.parseBoolean(args[50])));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.listaUsuariosFinales(Integer.parseInt(args[51])));
	        System.out.println("Server said: " + port2.obtenerListaServiciosCompleta());
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
