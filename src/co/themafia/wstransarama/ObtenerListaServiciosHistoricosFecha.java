
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para obtenerListaServiciosHistoricosFecha complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="obtenerListaServiciosHistoricosFecha"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tipoProyecto" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="trama" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenerListaServiciosHistoricosFecha", propOrder = {
    "tipoProyecto",
    "trama"
})
public class ObtenerListaServiciosHistoricosFecha {

    protected int tipoProyecto;
    protected String trama;

    /**
     * Obtiene el valor de la propiedad tipoProyecto.
     * 
     */
    public int getTipoProyecto() {
        return tipoProyecto;
    }

    /**
     * Define el valor de la propiedad tipoProyecto.
     * 
     */
    public void setTipoProyecto(int value) {
        this.tipoProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad trama.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrama() {
        return trama;
    }

    /**
     * Define el valor de la propiedad trama.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrama(String value) {
        this.trama = value;
    }

}
