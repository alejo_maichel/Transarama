
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para modificarTiempoMinimoYsancion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="modificarTiempoMinimoYsancion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tipoServicio" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="permiteInmediato" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tiempoMinimoReserva" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tiempoMinimoSancion" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modificarTiempoMinimoYsancion", propOrder = {
    "tipoServicio",
    "permiteInmediato",
    "tiempoMinimoReserva",
    "tiempoMinimoSancion"
})
public class ModificarTiempoMinimoYsancion {

    protected int tipoServicio;
    protected int permiteInmediato;
    protected int tiempoMinimoReserva;
    protected int tiempoMinimoSancion;

    /**
     * Obtiene el valor de la propiedad tipoServicio.
     * 
     */
    public int getTipoServicio() {
        return tipoServicio;
    }

    /**
     * Define el valor de la propiedad tipoServicio.
     * 
     */
    public void setTipoServicio(int value) {
        this.tipoServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad permiteInmediato.
     * 
     */
    public int getPermiteInmediato() {
        return permiteInmediato;
    }

    /**
     * Define el valor de la propiedad permiteInmediato.
     * 
     */
    public void setPermiteInmediato(int value) {
        this.permiteInmediato = value;
    }

    /**
     * Obtiene el valor de la propiedad tiempoMinimoReserva.
     * 
     */
    public int getTiempoMinimoReserva() {
        return tiempoMinimoReserva;
    }

    /**
     * Define el valor de la propiedad tiempoMinimoReserva.
     * 
     */
    public void setTiempoMinimoReserva(int value) {
        this.tiempoMinimoReserva = value;
    }

    /**
     * Obtiene el valor de la propiedad tiempoMinimoSancion.
     * 
     */
    public int getTiempoMinimoSancion() {
        return tiempoMinimoSancion;
    }

    /**
     * Define el valor de la propiedad tiempoMinimoSancion.
     * 
     */
    public void setTiempoMinimoSancion(int value) {
        this.tiempoMinimoSancion = value;
    }

}
