
package co.themafia.wstransarama;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.themafia.wstransarama package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ActualizarDireccionUsuario_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarDireccionUsuario");
    private final static QName _ActualizarDireccionUsuarioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarDireccionUsuarioResponse");
    private final static QName _ActualizarModificarUsuario_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarModificarUsuario");
    private final static QName _ActualizarModificarUsuarioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarModificarUsuarioResponse");
    private final static QName _ActualizarParametro_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarParametro");
    private final static QName _ActualizarParametroResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "actualizarParametroResponse");
    private final static QName _AgregarModificarConductor_QNAME = new QName("http://ws.fastcar.rwtec.com/", "agregarModificarConductor");
    private final static QName _AgregarModificarConductorResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "agregarModificarConductorResponse");
    private final static QName _AgregarModificarMovil_QNAME = new QName("http://ws.fastcar.rwtec.com/", "agregarModificarMovil");
    private final static QName _AgregarModificarMovilResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "agregarModificarMovilResponse");
    private final static QName _AnalizarTramaUnidades_QNAME = new QName("http://ws.fastcar.rwtec.com/", "analizarTramaUnidades");
    private final static QName _AnalizarTramaUnidadesResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "analizarTramaUnidadesResponse");
    private final static QName _AsociarConductorMoviles_QNAME = new QName("http://ws.fastcar.rwtec.com/", "asociarConductorMoviles");
    private final static QName _AsociarConductorMovilesResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "asociarConductorMovilesResponse");
    private final static QName _CancelarServicioCentral_QNAME = new QName("http://ws.fastcar.rwtec.com/", "cancelarServicioCentral");
    private final static QName _CancelarServicioCentralResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "cancelarServicioCentralResponse");
    private final static QName _ConsultarDatosUsuario_QNAME = new QName("http://ws.fastcar.rwtec.com/", "consultarDatosUsuario");
    private final static QName _ConsultarDatosUsuarioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "consultarDatosUsuarioResponse");
    private final static QName _ConsultarServicio_QNAME = new QName("http://ws.fastcar.rwtec.com/", "consultarServicio");
    private final static QName _ConsultarServicioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "consultarServicioResponse");
    private final static QName _CrearServicio_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicio");
    private final static QName _CrearServicioAvianca_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicioAvianca");
    private final static QName _CrearServicioAviancaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicioAviancaResponse");
    private final static QName _CrearServicioDatosServicio_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicioDatosServicio");
    private final static QName _CrearServicioDatosServicioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicioDatosServicioResponse");
    private final static QName _CrearServicioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "crearServicioResponse");
    private final static QName _EliminarDireccionUsuario_QNAME = new QName("http://ws.fastcar.rwtec.com/", "eliminarDireccionUsuario");
    private final static QName _EliminarDireccionUsuarioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "eliminarDireccionUsuarioResponse");
    private final static QName _EliminarRegistroProcesoServiciosAvianca_QNAME = new QName("http://ws.fastcar.rwtec.com/", "eliminarRegistroProcesoServiciosAvianca");
    private final static QName _EliminarRegistroProcesoServiciosAviancaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "eliminarRegistroProcesoServiciosAviancaResponse");
    private final static QName _EnviarTramaUnidad_QNAME = new QName("http://ws.fastcar.rwtec.com/", "enviarTramaUnidad");
    private final static QName _EnviarTramaUnidadResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "enviarTramaUnidadResponse");
    private final static QName _FuncionesOperativa_QNAME = new QName("http://ws.fastcar.rwtec.com/", "funcionesOperativa");
    private final static QName _FuncionesOperativaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "funcionesOperativaResponse");
    private final static QName _GeoReferenciarRuteoGeneral_QNAME = new QName("http://ws.fastcar.rwtec.com/", "geoReferenciarRuteoGeneral");
    private final static QName _GeoReferenciarRuteoGeneralResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "geoReferenciarRuteoGeneralResponse");
    private final static QName _ImprimirServiciosAvtivos_QNAME = new QName("http://ws.fastcar.rwtec.com/", "imprimirServiciosAvtivos");
    private final static QName _LevantarSancionConductor_QNAME = new QName("http://ws.fastcar.rwtec.com/", "levantarSancionConductor");
    private final static QName _LevantarSancionConductorResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "levantarSancionConductorResponse");
    private final static QName _LevantarSancionMovil_QNAME = new QName("http://ws.fastcar.rwtec.com/", "levantarSancionMovil");
    private final static QName _LevantarSancionMovilResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "levantarSancionMovilResponse");
    private final static QName _ListaServiciosAsignarAvianca_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listaServiciosAsignarAvianca");
    private final static QName _ListaServiciosAsignarAviancaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listaServiciosAsignarAviancaResponse");
    private final static QName _ListaUsuariosFinales_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listaUsuariosFinales");
    private final static QName _ListaUsuariosFinalesResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listaUsuariosFinalesResponse");
    private final static QName _ListarServiciosMotiva_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listarServiciosMotiva");
    private final static QName _ListarServiciosMotivaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "listarServiciosMotivaResponse");
    private final static QName _MensajesAlMovil_QNAME = new QName("http://ws.fastcar.rwtec.com/", "mensajesAlMovil");
    private final static QName _MensajesAlMovilResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "mensajesAlMovilResponse");
    private final static QName _MensajesGrupales_QNAME = new QName("http://ws.fastcar.rwtec.com/", "mensajesGrupales");
    private final static QName _MensajesGrupalesResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "mensajesGrupalesResponse");
    private final static QName _ModificarTiempoMinimoYsancion_QNAME = new QName("http://ws.fastcar.rwtec.com/", "modificarTiempoMinimoYsancion");
    private final static QName _ModificarTiempoMinimoYsancionResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "modificarTiempoMinimoYsancionResponse");
    private final static QName _ObtenerDatosConductor_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerDatosConductor");
    private final static QName _ObtenerDatosConductorResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerDatosConductorResponse");
    private final static QName _ObtenerDatosMovil_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerDatosMovil");
    private final static QName _ObtenerDatosMovilResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerDatosMovilResponse");
    private final static QName _ObtenerListaMovilesConductoresWS_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaMovilesConductoresWS");
    private final static QName _ObtenerListaMovilesConductoresWSResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaMovilesConductoresWSResponse");
    private final static QName _ObtenerListaServiciosCompleta_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosCompleta");
    private final static QName _ObtenerListaServiciosCompletaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosCompletaResponse");
    private final static QName _ObtenerListaServiciosHistoricosCompleta_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosHistoricosCompleta");
    private final static QName _ObtenerListaServiciosHistoricosCompletaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosHistoricosCompletaResponse");
    private final static QName _ObtenerListaServiciosHistoricosFecha_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosHistoricosFecha");
    private final static QName _ObtenerListaServiciosHistoricosFechaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosHistoricosFechaResponse");
    private final static QName _ObtenerListaServiciosMemoria_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosMemoria");
    private final static QName _ObtenerListaServiciosMemoriaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaServiciosMemoriaResponse");
    private final static QName _ObtenerListaTipoServicio_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaTipoServicio");
    private final static QName _ObtenerListaTipoServicioResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerListaTipoServicioResponse");
    private final static QName _ObtenerServicioXtel_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerServicioXtel");
    private final static QName _ObtenerServicioXtelResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerServicioXtelResponse");
    private final static QName _ObtenerUsuarioSistema_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerUsuarioSistema");
    private final static QName _ObtenerUsuarioSistemaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "obtenerUsuarioSistemaResponse");
    private final static QName _RecibirCargaTripulacionAvianca_QNAME = new QName("http://ws.fastcar.rwtec.com/", "recibirCargaTripulacionAvianca");
    private final static QName _RecibirCargaTripulacionAviancaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "recibirCargaTripulacionAviancaResponse");
    private final static QName _RecibirDatosAvianca_QNAME = new QName("http://ws.fastcar.rwtec.com/", "recibirDatosAvianca");
    private final static QName _RecibirDatosAviancaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "recibirDatosAviancaResponse");
    private final static QName _VerificarVersionActual_QNAME = new QName("http://ws.fastcar.rwtec.com/", "verificarVersionActual");
    private final static QName _VerificarVersionActualResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "verificarVersionActualResponse");
    private final static QName _WebLogInUsuarioSistema_QNAME = new QName("http://ws.fastcar.rwtec.com/", "webLogInUsuarioSistema");
    private final static QName _WebLogInUsuarioSistemaResponse_QNAME = new QName("http://ws.fastcar.rwtec.com/", "webLogInUsuarioSistemaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.themafia.wstransarama
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActualizarDireccionUsuario }
     * 
     */
    public ActualizarDireccionUsuario createActualizarDireccionUsuario() {
        return new ActualizarDireccionUsuario();
    }

    /**
     * Create an instance of {@link ActualizarDireccionUsuarioResponse }
     * 
     */
    public ActualizarDireccionUsuarioResponse createActualizarDireccionUsuarioResponse() {
        return new ActualizarDireccionUsuarioResponse();
    }

    /**
     * Create an instance of {@link ActualizarModificarUsuario }
     * 
     */
    public ActualizarModificarUsuario createActualizarModificarUsuario() {
        return new ActualizarModificarUsuario();
    }

    /**
     * Create an instance of {@link ActualizarModificarUsuarioResponse }
     * 
     */
    public ActualizarModificarUsuarioResponse createActualizarModificarUsuarioResponse() {
        return new ActualizarModificarUsuarioResponse();
    }

    /**
     * Create an instance of {@link ActualizarParametro }
     * 
     */
    public ActualizarParametro createActualizarParametro() {
        return new ActualizarParametro();
    }

    /**
     * Create an instance of {@link ActualizarParametroResponse }
     * 
     */
    public ActualizarParametroResponse createActualizarParametroResponse() {
        return new ActualizarParametroResponse();
    }

    /**
     * Create an instance of {@link AgregarModificarConductor }
     * 
     */
    public AgregarModificarConductor createAgregarModificarConductor() {
        return new AgregarModificarConductor();
    }

    /**
     * Create an instance of {@link AgregarModificarConductorResponse }
     * 
     */
    public AgregarModificarConductorResponse createAgregarModificarConductorResponse() {
        return new AgregarModificarConductorResponse();
    }

    /**
     * Create an instance of {@link AgregarModificarMovil }
     * 
     */
    public AgregarModificarMovil createAgregarModificarMovil() {
        return new AgregarModificarMovil();
    }

    /**
     * Create an instance of {@link AgregarModificarMovilResponse }
     * 
     */
    public AgregarModificarMovilResponse createAgregarModificarMovilResponse() {
        return new AgregarModificarMovilResponse();
    }

    /**
     * Create an instance of {@link AnalizarTramaUnidades }
     * 
     */
    public AnalizarTramaUnidades createAnalizarTramaUnidades() {
        return new AnalizarTramaUnidades();
    }

    /**
     * Create an instance of {@link AnalizarTramaUnidadesResponse }
     * 
     */
    public AnalizarTramaUnidadesResponse createAnalizarTramaUnidadesResponse() {
        return new AnalizarTramaUnidadesResponse();
    }

    /**
     * Create an instance of {@link AsociarConductorMoviles }
     * 
     */
    public AsociarConductorMoviles createAsociarConductorMoviles() {
        return new AsociarConductorMoviles();
    }

    /**
     * Create an instance of {@link AsociarConductorMovilesResponse }
     * 
     */
    public AsociarConductorMovilesResponse createAsociarConductorMovilesResponse() {
        return new AsociarConductorMovilesResponse();
    }

    /**
     * Create an instance of {@link CancelarServicioCentral }
     * 
     */
    public CancelarServicioCentral createCancelarServicioCentral() {
        return new CancelarServicioCentral();
    }

    /**
     * Create an instance of {@link CancelarServicioCentralResponse }
     * 
     */
    public CancelarServicioCentralResponse createCancelarServicioCentralResponse() {
        return new CancelarServicioCentralResponse();
    }

    /**
     * Create an instance of {@link ConsultarDatosUsuario }
     * 
     */
    public ConsultarDatosUsuario createConsultarDatosUsuario() {
        return new ConsultarDatosUsuario();
    }

    /**
     * Create an instance of {@link ConsultarDatosUsuarioResponse }
     * 
     */
    public ConsultarDatosUsuarioResponse createConsultarDatosUsuarioResponse() {
        return new ConsultarDatosUsuarioResponse();
    }

    /**
     * Create an instance of {@link ConsultarServicio }
     * 
     */
    public ConsultarServicio createConsultarServicio() {
        return new ConsultarServicio();
    }

    /**
     * Create an instance of {@link ConsultarServicioResponse }
     * 
     */
    public ConsultarServicioResponse createConsultarServicioResponse() {
        return new ConsultarServicioResponse();
    }

    /**
     * Create an instance of {@link CrearServicio }
     * 
     */
    public CrearServicio createCrearServicio() {
        return new CrearServicio();
    }

    /**
     * Create an instance of {@link CrearServicioAvianca }
     * 
     */
    public CrearServicioAvianca createCrearServicioAvianca() {
        return new CrearServicioAvianca();
    }

    /**
     * Create an instance of {@link CrearServicioAviancaResponse }
     * 
     */
    public CrearServicioAviancaResponse createCrearServicioAviancaResponse() {
        return new CrearServicioAviancaResponse();
    }

    /**
     * Create an instance of {@link CrearServicioDatosServicio }
     * 
     */
    public CrearServicioDatosServicio createCrearServicioDatosServicio() {
        return new CrearServicioDatosServicio();
    }

    /**
     * Create an instance of {@link CrearServicioDatosServicioResponse }
     * 
     */
    public CrearServicioDatosServicioResponse createCrearServicioDatosServicioResponse() {
        return new CrearServicioDatosServicioResponse();
    }

    /**
     * Create an instance of {@link CrearServicioResponse }
     * 
     */
    public CrearServicioResponse createCrearServicioResponse() {
        return new CrearServicioResponse();
    }

    /**
     * Create an instance of {@link EliminarDireccionUsuario }
     * 
     */
    public EliminarDireccionUsuario createEliminarDireccionUsuario() {
        return new EliminarDireccionUsuario();
    }

    /**
     * Create an instance of {@link EliminarDireccionUsuarioResponse }
     * 
     */
    public EliminarDireccionUsuarioResponse createEliminarDireccionUsuarioResponse() {
        return new EliminarDireccionUsuarioResponse();
    }

    /**
     * Create an instance of {@link EliminarRegistroProcesoServiciosAvianca }
     * 
     */
    public EliminarRegistroProcesoServiciosAvianca createEliminarRegistroProcesoServiciosAvianca() {
        return new EliminarRegistroProcesoServiciosAvianca();
    }

    /**
     * Create an instance of {@link EliminarRegistroProcesoServiciosAviancaResponse }
     * 
     */
    public EliminarRegistroProcesoServiciosAviancaResponse createEliminarRegistroProcesoServiciosAviancaResponse() {
        return new EliminarRegistroProcesoServiciosAviancaResponse();
    }

    /**
     * Create an instance of {@link EnviarTramaUnidad }
     * 
     */
    public EnviarTramaUnidad createEnviarTramaUnidad() {
        return new EnviarTramaUnidad();
    }

    /**
     * Create an instance of {@link EnviarTramaUnidadResponse }
     * 
     */
    public EnviarTramaUnidadResponse createEnviarTramaUnidadResponse() {
        return new EnviarTramaUnidadResponse();
    }

    /**
     * Create an instance of {@link FuncionesOperativa }
     * 
     */
    public FuncionesOperativa createFuncionesOperativa() {
        return new FuncionesOperativa();
    }

    /**
     * Create an instance of {@link FuncionesOperativaResponse }
     * 
     */
    public FuncionesOperativaResponse createFuncionesOperativaResponse() {
        return new FuncionesOperativaResponse();
    }

    /**
     * Create an instance of {@link GeoReferenciarRuteoGeneral }
     * 
     */
    public GeoReferenciarRuteoGeneral createGeoReferenciarRuteoGeneral() {
        return new GeoReferenciarRuteoGeneral();
    }

    /**
     * Create an instance of {@link GeoReferenciarRuteoGeneralResponse }
     * 
     */
    public GeoReferenciarRuteoGeneralResponse createGeoReferenciarRuteoGeneralResponse() {
        return new GeoReferenciarRuteoGeneralResponse();
    }

    /**
     * Create an instance of {@link ImprimirServiciosAvtivos }
     * 
     */
    public ImprimirServiciosAvtivos createImprimirServiciosAvtivos() {
        return new ImprimirServiciosAvtivos();
    }

    /**
     * Create an instance of {@link LevantarSancionConductor }
     * 
     */
    public LevantarSancionConductor createLevantarSancionConductor() {
        return new LevantarSancionConductor();
    }

    /**
     * Create an instance of {@link LevantarSancionConductorResponse }
     * 
     */
    public LevantarSancionConductorResponse createLevantarSancionConductorResponse() {
        return new LevantarSancionConductorResponse();
    }

    /**
     * Create an instance of {@link LevantarSancionMovil }
     * 
     */
    public LevantarSancionMovil createLevantarSancionMovil() {
        return new LevantarSancionMovil();
    }

    /**
     * Create an instance of {@link LevantarSancionMovilResponse }
     * 
     */
    public LevantarSancionMovilResponse createLevantarSancionMovilResponse() {
        return new LevantarSancionMovilResponse();
    }

    /**
     * Create an instance of {@link ListaServiciosAsignarAvianca }
     * 
     */
    public ListaServiciosAsignarAvianca createListaServiciosAsignarAvianca() {
        return new ListaServiciosAsignarAvianca();
    }

    /**
     * Create an instance of {@link ListaServiciosAsignarAviancaResponse }
     * 
     */
    public ListaServiciosAsignarAviancaResponse createListaServiciosAsignarAviancaResponse() {
        return new ListaServiciosAsignarAviancaResponse();
    }

    /**
     * Create an instance of {@link ListaUsuariosFinales }
     * 
     */
    public ListaUsuariosFinales createListaUsuariosFinales() {
        return new ListaUsuariosFinales();
    }

    /**
     * Create an instance of {@link ListaUsuariosFinalesResponse }
     * 
     */
    public ListaUsuariosFinalesResponse createListaUsuariosFinalesResponse() {
        return new ListaUsuariosFinalesResponse();
    }

    /**
     * Create an instance of {@link ListarServiciosMotiva }
     * 
     */
    public ListarServiciosMotiva createListarServiciosMotiva() {
        return new ListarServiciosMotiva();
    }

    /**
     * Create an instance of {@link ListarServiciosMotivaResponse }
     * 
     */
    public ListarServiciosMotivaResponse createListarServiciosMotivaResponse() {
        return new ListarServiciosMotivaResponse();
    }

    /**
     * Create an instance of {@link MensajesAlMovil }
     * 
     */
    public MensajesAlMovil createMensajesAlMovil() {
        return new MensajesAlMovil();
    }

    /**
     * Create an instance of {@link MensajesAlMovilResponse }
     * 
     */
    public MensajesAlMovilResponse createMensajesAlMovilResponse() {
        return new MensajesAlMovilResponse();
    }

    /**
     * Create an instance of {@link MensajesGrupales }
     * 
     */
    public MensajesGrupales createMensajesGrupales() {
        return new MensajesGrupales();
    }

    /**
     * Create an instance of {@link MensajesGrupalesResponse }
     * 
     */
    public MensajesGrupalesResponse createMensajesGrupalesResponse() {
        return new MensajesGrupalesResponse();
    }

    /**
     * Create an instance of {@link ModificarTiempoMinimoYsancion }
     * 
     */
    public ModificarTiempoMinimoYsancion createModificarTiempoMinimoYsancion() {
        return new ModificarTiempoMinimoYsancion();
    }

    /**
     * Create an instance of {@link ModificarTiempoMinimoYsancionResponse }
     * 
     */
    public ModificarTiempoMinimoYsancionResponse createModificarTiempoMinimoYsancionResponse() {
        return new ModificarTiempoMinimoYsancionResponse();
    }

    /**
     * Create an instance of {@link ObtenerDatosConductor }
     * 
     */
    public ObtenerDatosConductor createObtenerDatosConductor() {
        return new ObtenerDatosConductor();
    }

    /**
     * Create an instance of {@link ObtenerDatosConductorResponse }
     * 
     */
    public ObtenerDatosConductorResponse createObtenerDatosConductorResponse() {
        return new ObtenerDatosConductorResponse();
    }

    /**
     * Create an instance of {@link ObtenerDatosMovil }
     * 
     */
    public ObtenerDatosMovil createObtenerDatosMovil() {
        return new ObtenerDatosMovil();
    }

    /**
     * Create an instance of {@link ObtenerDatosMovilResponse }
     * 
     */
    public ObtenerDatosMovilResponse createObtenerDatosMovilResponse() {
        return new ObtenerDatosMovilResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaMovilesConductoresWS }
     * 
     */
    public ObtenerListaMovilesConductoresWS createObtenerListaMovilesConductoresWS() {
        return new ObtenerListaMovilesConductoresWS();
    }

    /**
     * Create an instance of {@link ObtenerListaMovilesConductoresWSResponse }
     * 
     */
    public ObtenerListaMovilesConductoresWSResponse createObtenerListaMovilesConductoresWSResponse() {
        return new ObtenerListaMovilesConductoresWSResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosCompleta }
     * 
     */
    public ObtenerListaServiciosCompleta createObtenerListaServiciosCompleta() {
        return new ObtenerListaServiciosCompleta();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosCompletaResponse }
     * 
     */
    public ObtenerListaServiciosCompletaResponse createObtenerListaServiciosCompletaResponse() {
        return new ObtenerListaServiciosCompletaResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosHistoricosCompleta }
     * 
     */
    public ObtenerListaServiciosHistoricosCompleta createObtenerListaServiciosHistoricosCompleta() {
        return new ObtenerListaServiciosHistoricosCompleta();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosHistoricosCompletaResponse }
     * 
     */
    public ObtenerListaServiciosHistoricosCompletaResponse createObtenerListaServiciosHistoricosCompletaResponse() {
        return new ObtenerListaServiciosHistoricosCompletaResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosHistoricosFecha }
     * 
     */
    public ObtenerListaServiciosHistoricosFecha createObtenerListaServiciosHistoricosFecha() {
        return new ObtenerListaServiciosHistoricosFecha();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosHistoricosFechaResponse }
     * 
     */
    public ObtenerListaServiciosHistoricosFechaResponse createObtenerListaServiciosHistoricosFechaResponse() {
        return new ObtenerListaServiciosHistoricosFechaResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosMemoria }
     * 
     */
    public ObtenerListaServiciosMemoria createObtenerListaServiciosMemoria() {
        return new ObtenerListaServiciosMemoria();
    }

    /**
     * Create an instance of {@link ObtenerListaServiciosMemoriaResponse }
     * 
     */
    public ObtenerListaServiciosMemoriaResponse createObtenerListaServiciosMemoriaResponse() {
        return new ObtenerListaServiciosMemoriaResponse();
    }

    /**
     * Create an instance of {@link ObtenerListaTipoServicio }
     * 
     */
    public ObtenerListaTipoServicio createObtenerListaTipoServicio() {
        return new ObtenerListaTipoServicio();
    }

    /**
     * Create an instance of {@link ObtenerListaTipoServicioResponse }
     * 
     */
    public ObtenerListaTipoServicioResponse createObtenerListaTipoServicioResponse() {
        return new ObtenerListaTipoServicioResponse();
    }

    /**
     * Create an instance of {@link ObtenerServicioXtel }
     * 
     */
    public ObtenerServicioXtel createObtenerServicioXtel() {
        return new ObtenerServicioXtel();
    }

    /**
     * Create an instance of {@link ObtenerServicioXtelResponse }
     * 
     */
    public ObtenerServicioXtelResponse createObtenerServicioXtelResponse() {
        return new ObtenerServicioXtelResponse();
    }

    /**
     * Create an instance of {@link ObtenerUsuarioSistema }
     * 
     */
    public ObtenerUsuarioSistema createObtenerUsuarioSistema() {
        return new ObtenerUsuarioSistema();
    }

    /**
     * Create an instance of {@link ObtenerUsuarioSistemaResponse }
     * 
     */
    public ObtenerUsuarioSistemaResponse createObtenerUsuarioSistemaResponse() {
        return new ObtenerUsuarioSistemaResponse();
    }

    /**
     * Create an instance of {@link RecibirCargaTripulacionAvianca }
     * 
     */
    public RecibirCargaTripulacionAvianca createRecibirCargaTripulacionAvianca() {
        return new RecibirCargaTripulacionAvianca();
    }

    /**
     * Create an instance of {@link RecibirCargaTripulacionAviancaResponse }
     * 
     */
    public RecibirCargaTripulacionAviancaResponse createRecibirCargaTripulacionAviancaResponse() {
        return new RecibirCargaTripulacionAviancaResponse();
    }

    /**
     * Create an instance of {@link RecibirDatosAvianca }
     * 
     */
    public RecibirDatosAvianca createRecibirDatosAvianca() {
        return new RecibirDatosAvianca();
    }

    /**
     * Create an instance of {@link RecibirDatosAviancaResponse }
     * 
     */
    public RecibirDatosAviancaResponse createRecibirDatosAviancaResponse() {
        return new RecibirDatosAviancaResponse();
    }

    /**
     * Create an instance of {@link VerificarVersionActual }
     * 
     */
    public VerificarVersionActual createVerificarVersionActual() {
        return new VerificarVersionActual();
    }

    /**
     * Create an instance of {@link VerificarVersionActualResponse }
     * 
     */
    public VerificarVersionActualResponse createVerificarVersionActualResponse() {
        return new VerificarVersionActualResponse();
    }

    /**
     * Create an instance of {@link WebLogInUsuarioSistema }
     * 
     */
    public WebLogInUsuarioSistema createWebLogInUsuarioSistema() {
        return new WebLogInUsuarioSistema();
    }

    /**
     * Create an instance of {@link WebLogInUsuarioSistemaResponse }
     * 
     */
    public WebLogInUsuarioSistemaResponse createWebLogInUsuarioSistemaResponse() {
        return new WebLogInUsuarioSistemaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarDireccionUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarDireccionUsuario")
    public JAXBElement<ActualizarDireccionUsuario> createActualizarDireccionUsuario(ActualizarDireccionUsuario value) {
        return new JAXBElement<ActualizarDireccionUsuario>(_ActualizarDireccionUsuario_QNAME, ActualizarDireccionUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarDireccionUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarDireccionUsuarioResponse")
    public JAXBElement<ActualizarDireccionUsuarioResponse> createActualizarDireccionUsuarioResponse(ActualizarDireccionUsuarioResponse value) {
        return new JAXBElement<ActualizarDireccionUsuarioResponse>(_ActualizarDireccionUsuarioResponse_QNAME, ActualizarDireccionUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarModificarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarModificarUsuario")
    public JAXBElement<ActualizarModificarUsuario> createActualizarModificarUsuario(ActualizarModificarUsuario value) {
        return new JAXBElement<ActualizarModificarUsuario>(_ActualizarModificarUsuario_QNAME, ActualizarModificarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarModificarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarModificarUsuarioResponse")
    public JAXBElement<ActualizarModificarUsuarioResponse> createActualizarModificarUsuarioResponse(ActualizarModificarUsuarioResponse value) {
        return new JAXBElement<ActualizarModificarUsuarioResponse>(_ActualizarModificarUsuarioResponse_QNAME, ActualizarModificarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarParametro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarParametro")
    public JAXBElement<ActualizarParametro> createActualizarParametro(ActualizarParametro value) {
        return new JAXBElement<ActualizarParametro>(_ActualizarParametro_QNAME, ActualizarParametro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualizarParametroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "actualizarParametroResponse")
    public JAXBElement<ActualizarParametroResponse> createActualizarParametroResponse(ActualizarParametroResponse value) {
        return new JAXBElement<ActualizarParametroResponse>(_ActualizarParametroResponse_QNAME, ActualizarParametroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarModificarConductor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "agregarModificarConductor")
    public JAXBElement<AgregarModificarConductor> createAgregarModificarConductor(AgregarModificarConductor value) {
        return new JAXBElement<AgregarModificarConductor>(_AgregarModificarConductor_QNAME, AgregarModificarConductor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarModificarConductorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "agregarModificarConductorResponse")
    public JAXBElement<AgregarModificarConductorResponse> createAgregarModificarConductorResponse(AgregarModificarConductorResponse value) {
        return new JAXBElement<AgregarModificarConductorResponse>(_AgregarModificarConductorResponse_QNAME, AgregarModificarConductorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarModificarMovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "agregarModificarMovil")
    public JAXBElement<AgregarModificarMovil> createAgregarModificarMovil(AgregarModificarMovil value) {
        return new JAXBElement<AgregarModificarMovil>(_AgregarModificarMovil_QNAME, AgregarModificarMovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarModificarMovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "agregarModificarMovilResponse")
    public JAXBElement<AgregarModificarMovilResponse> createAgregarModificarMovilResponse(AgregarModificarMovilResponse value) {
        return new JAXBElement<AgregarModificarMovilResponse>(_AgregarModificarMovilResponse_QNAME, AgregarModificarMovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnalizarTramaUnidades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "analizarTramaUnidades")
    public JAXBElement<AnalizarTramaUnidades> createAnalizarTramaUnidades(AnalizarTramaUnidades value) {
        return new JAXBElement<AnalizarTramaUnidades>(_AnalizarTramaUnidades_QNAME, AnalizarTramaUnidades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnalizarTramaUnidadesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "analizarTramaUnidadesResponse")
    public JAXBElement<AnalizarTramaUnidadesResponse> createAnalizarTramaUnidadesResponse(AnalizarTramaUnidadesResponse value) {
        return new JAXBElement<AnalizarTramaUnidadesResponse>(_AnalizarTramaUnidadesResponse_QNAME, AnalizarTramaUnidadesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsociarConductorMoviles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "asociarConductorMoviles")
    public JAXBElement<AsociarConductorMoviles> createAsociarConductorMoviles(AsociarConductorMoviles value) {
        return new JAXBElement<AsociarConductorMoviles>(_AsociarConductorMoviles_QNAME, AsociarConductorMoviles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsociarConductorMovilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "asociarConductorMovilesResponse")
    public JAXBElement<AsociarConductorMovilesResponse> createAsociarConductorMovilesResponse(AsociarConductorMovilesResponse value) {
        return new JAXBElement<AsociarConductorMovilesResponse>(_AsociarConductorMovilesResponse_QNAME, AsociarConductorMovilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarServicioCentral }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "cancelarServicioCentral")
    public JAXBElement<CancelarServicioCentral> createCancelarServicioCentral(CancelarServicioCentral value) {
        return new JAXBElement<CancelarServicioCentral>(_CancelarServicioCentral_QNAME, CancelarServicioCentral.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarServicioCentralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "cancelarServicioCentralResponse")
    public JAXBElement<CancelarServicioCentralResponse> createCancelarServicioCentralResponse(CancelarServicioCentralResponse value) {
        return new JAXBElement<CancelarServicioCentralResponse>(_CancelarServicioCentralResponse_QNAME, CancelarServicioCentralResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarDatosUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "consultarDatosUsuario")
    public JAXBElement<ConsultarDatosUsuario> createConsultarDatosUsuario(ConsultarDatosUsuario value) {
        return new JAXBElement<ConsultarDatosUsuario>(_ConsultarDatosUsuario_QNAME, ConsultarDatosUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarDatosUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "consultarDatosUsuarioResponse")
    public JAXBElement<ConsultarDatosUsuarioResponse> createConsultarDatosUsuarioResponse(ConsultarDatosUsuarioResponse value) {
        return new JAXBElement<ConsultarDatosUsuarioResponse>(_ConsultarDatosUsuarioResponse_QNAME, ConsultarDatosUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarServicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "consultarServicio")
    public JAXBElement<ConsultarServicio> createConsultarServicio(ConsultarServicio value) {
        return new JAXBElement<ConsultarServicio>(_ConsultarServicio_QNAME, ConsultarServicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarServicioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "consultarServicioResponse")
    public JAXBElement<ConsultarServicioResponse> createConsultarServicioResponse(ConsultarServicioResponse value) {
        return new JAXBElement<ConsultarServicioResponse>(_ConsultarServicioResponse_QNAME, ConsultarServicioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicio")
    public JAXBElement<CrearServicio> createCrearServicio(CrearServicio value) {
        return new JAXBElement<CrearServicio>(_CrearServicio_QNAME, CrearServicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicioAvianca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicioAvianca")
    public JAXBElement<CrearServicioAvianca> createCrearServicioAvianca(CrearServicioAvianca value) {
        return new JAXBElement<CrearServicioAvianca>(_CrearServicioAvianca_QNAME, CrearServicioAvianca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicioAviancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicioAviancaResponse")
    public JAXBElement<CrearServicioAviancaResponse> createCrearServicioAviancaResponse(CrearServicioAviancaResponse value) {
        return new JAXBElement<CrearServicioAviancaResponse>(_CrearServicioAviancaResponse_QNAME, CrearServicioAviancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicioDatosServicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicioDatosServicio")
    public JAXBElement<CrearServicioDatosServicio> createCrearServicioDatosServicio(CrearServicioDatosServicio value) {
        return new JAXBElement<CrearServicioDatosServicio>(_CrearServicioDatosServicio_QNAME, CrearServicioDatosServicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicioDatosServicioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicioDatosServicioResponse")
    public JAXBElement<CrearServicioDatosServicioResponse> createCrearServicioDatosServicioResponse(CrearServicioDatosServicioResponse value) {
        return new JAXBElement<CrearServicioDatosServicioResponse>(_CrearServicioDatosServicioResponse_QNAME, CrearServicioDatosServicioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearServicioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "crearServicioResponse")
    public JAXBElement<CrearServicioResponse> createCrearServicioResponse(CrearServicioResponse value) {
        return new JAXBElement<CrearServicioResponse>(_CrearServicioResponse_QNAME, CrearServicioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarDireccionUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "eliminarDireccionUsuario")
    public JAXBElement<EliminarDireccionUsuario> createEliminarDireccionUsuario(EliminarDireccionUsuario value) {
        return new JAXBElement<EliminarDireccionUsuario>(_EliminarDireccionUsuario_QNAME, EliminarDireccionUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarDireccionUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "eliminarDireccionUsuarioResponse")
    public JAXBElement<EliminarDireccionUsuarioResponse> createEliminarDireccionUsuarioResponse(EliminarDireccionUsuarioResponse value) {
        return new JAXBElement<EliminarDireccionUsuarioResponse>(_EliminarDireccionUsuarioResponse_QNAME, EliminarDireccionUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarRegistroProcesoServiciosAvianca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "eliminarRegistroProcesoServiciosAvianca")
    public JAXBElement<EliminarRegistroProcesoServiciosAvianca> createEliminarRegistroProcesoServiciosAvianca(EliminarRegistroProcesoServiciosAvianca value) {
        return new JAXBElement<EliminarRegistroProcesoServiciosAvianca>(_EliminarRegistroProcesoServiciosAvianca_QNAME, EliminarRegistroProcesoServiciosAvianca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarRegistroProcesoServiciosAviancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "eliminarRegistroProcesoServiciosAviancaResponse")
    public JAXBElement<EliminarRegistroProcesoServiciosAviancaResponse> createEliminarRegistroProcesoServiciosAviancaResponse(EliminarRegistroProcesoServiciosAviancaResponse value) {
        return new JAXBElement<EliminarRegistroProcesoServiciosAviancaResponse>(_EliminarRegistroProcesoServiciosAviancaResponse_QNAME, EliminarRegistroProcesoServiciosAviancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarTramaUnidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "enviarTramaUnidad")
    public JAXBElement<EnviarTramaUnidad> createEnviarTramaUnidad(EnviarTramaUnidad value) {
        return new JAXBElement<EnviarTramaUnidad>(_EnviarTramaUnidad_QNAME, EnviarTramaUnidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarTramaUnidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "enviarTramaUnidadResponse")
    public JAXBElement<EnviarTramaUnidadResponse> createEnviarTramaUnidadResponse(EnviarTramaUnidadResponse value) {
        return new JAXBElement<EnviarTramaUnidadResponse>(_EnviarTramaUnidadResponse_QNAME, EnviarTramaUnidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionesOperativa }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "funcionesOperativa")
    public JAXBElement<FuncionesOperativa> createFuncionesOperativa(FuncionesOperativa value) {
        return new JAXBElement<FuncionesOperativa>(_FuncionesOperativa_QNAME, FuncionesOperativa.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionesOperativaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "funcionesOperativaResponse")
    public JAXBElement<FuncionesOperativaResponse> createFuncionesOperativaResponse(FuncionesOperativaResponse value) {
        return new JAXBElement<FuncionesOperativaResponse>(_FuncionesOperativaResponse_QNAME, FuncionesOperativaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeoReferenciarRuteoGeneral }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "geoReferenciarRuteoGeneral")
    public JAXBElement<GeoReferenciarRuteoGeneral> createGeoReferenciarRuteoGeneral(GeoReferenciarRuteoGeneral value) {
        return new JAXBElement<GeoReferenciarRuteoGeneral>(_GeoReferenciarRuteoGeneral_QNAME, GeoReferenciarRuteoGeneral.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeoReferenciarRuteoGeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "geoReferenciarRuteoGeneralResponse")
    public JAXBElement<GeoReferenciarRuteoGeneralResponse> createGeoReferenciarRuteoGeneralResponse(GeoReferenciarRuteoGeneralResponse value) {
        return new JAXBElement<GeoReferenciarRuteoGeneralResponse>(_GeoReferenciarRuteoGeneralResponse_QNAME, GeoReferenciarRuteoGeneralResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImprimirServiciosAvtivos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "imprimirServiciosAvtivos")
    public JAXBElement<ImprimirServiciosAvtivos> createImprimirServiciosAvtivos(ImprimirServiciosAvtivos value) {
        return new JAXBElement<ImprimirServiciosAvtivos>(_ImprimirServiciosAvtivos_QNAME, ImprimirServiciosAvtivos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevantarSancionConductor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "levantarSancionConductor")
    public JAXBElement<LevantarSancionConductor> createLevantarSancionConductor(LevantarSancionConductor value) {
        return new JAXBElement<LevantarSancionConductor>(_LevantarSancionConductor_QNAME, LevantarSancionConductor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevantarSancionConductorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "levantarSancionConductorResponse")
    public JAXBElement<LevantarSancionConductorResponse> createLevantarSancionConductorResponse(LevantarSancionConductorResponse value) {
        return new JAXBElement<LevantarSancionConductorResponse>(_LevantarSancionConductorResponse_QNAME, LevantarSancionConductorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevantarSancionMovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "levantarSancionMovil")
    public JAXBElement<LevantarSancionMovil> createLevantarSancionMovil(LevantarSancionMovil value) {
        return new JAXBElement<LevantarSancionMovil>(_LevantarSancionMovil_QNAME, LevantarSancionMovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevantarSancionMovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "levantarSancionMovilResponse")
    public JAXBElement<LevantarSancionMovilResponse> createLevantarSancionMovilResponse(LevantarSancionMovilResponse value) {
        return new JAXBElement<LevantarSancionMovilResponse>(_LevantarSancionMovilResponse_QNAME, LevantarSancionMovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaServiciosAsignarAvianca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listaServiciosAsignarAvianca")
    public JAXBElement<ListaServiciosAsignarAvianca> createListaServiciosAsignarAvianca(ListaServiciosAsignarAvianca value) {
        return new JAXBElement<ListaServiciosAsignarAvianca>(_ListaServiciosAsignarAvianca_QNAME, ListaServiciosAsignarAvianca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaServiciosAsignarAviancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listaServiciosAsignarAviancaResponse")
    public JAXBElement<ListaServiciosAsignarAviancaResponse> createListaServiciosAsignarAviancaResponse(ListaServiciosAsignarAviancaResponse value) {
        return new JAXBElement<ListaServiciosAsignarAviancaResponse>(_ListaServiciosAsignarAviancaResponse_QNAME, ListaServiciosAsignarAviancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaUsuariosFinales }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listaUsuariosFinales")
    public JAXBElement<ListaUsuariosFinales> createListaUsuariosFinales(ListaUsuariosFinales value) {
        return new JAXBElement<ListaUsuariosFinales>(_ListaUsuariosFinales_QNAME, ListaUsuariosFinales.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaUsuariosFinalesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listaUsuariosFinalesResponse")
    public JAXBElement<ListaUsuariosFinalesResponse> createListaUsuariosFinalesResponse(ListaUsuariosFinalesResponse value) {
        return new JAXBElement<ListaUsuariosFinalesResponse>(_ListaUsuariosFinalesResponse_QNAME, ListaUsuariosFinalesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServiciosMotiva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listarServiciosMotiva")
    public JAXBElement<ListarServiciosMotiva> createListarServiciosMotiva(ListarServiciosMotiva value) {
        return new JAXBElement<ListarServiciosMotiva>(_ListarServiciosMotiva_QNAME, ListarServiciosMotiva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServiciosMotivaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "listarServiciosMotivaResponse")
    public JAXBElement<ListarServiciosMotivaResponse> createListarServiciosMotivaResponse(ListarServiciosMotivaResponse value) {
        return new JAXBElement<ListarServiciosMotivaResponse>(_ListarServiciosMotivaResponse_QNAME, ListarServiciosMotivaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MensajesAlMovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "mensajesAlMovil")
    public JAXBElement<MensajesAlMovil> createMensajesAlMovil(MensajesAlMovil value) {
        return new JAXBElement<MensajesAlMovil>(_MensajesAlMovil_QNAME, MensajesAlMovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MensajesAlMovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "mensajesAlMovilResponse")
    public JAXBElement<MensajesAlMovilResponse> createMensajesAlMovilResponse(MensajesAlMovilResponse value) {
        return new JAXBElement<MensajesAlMovilResponse>(_MensajesAlMovilResponse_QNAME, MensajesAlMovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MensajesGrupales }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "mensajesGrupales")
    public JAXBElement<MensajesGrupales> createMensajesGrupales(MensajesGrupales value) {
        return new JAXBElement<MensajesGrupales>(_MensajesGrupales_QNAME, MensajesGrupales.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MensajesGrupalesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "mensajesGrupalesResponse")
    public JAXBElement<MensajesGrupalesResponse> createMensajesGrupalesResponse(MensajesGrupalesResponse value) {
        return new JAXBElement<MensajesGrupalesResponse>(_MensajesGrupalesResponse_QNAME, MensajesGrupalesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarTiempoMinimoYsancion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "modificarTiempoMinimoYsancion")
    public JAXBElement<ModificarTiempoMinimoYsancion> createModificarTiempoMinimoYsancion(ModificarTiempoMinimoYsancion value) {
        return new JAXBElement<ModificarTiempoMinimoYsancion>(_ModificarTiempoMinimoYsancion_QNAME, ModificarTiempoMinimoYsancion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarTiempoMinimoYsancionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "modificarTiempoMinimoYsancionResponse")
    public JAXBElement<ModificarTiempoMinimoYsancionResponse> createModificarTiempoMinimoYsancionResponse(ModificarTiempoMinimoYsancionResponse value) {
        return new JAXBElement<ModificarTiempoMinimoYsancionResponse>(_ModificarTiempoMinimoYsancionResponse_QNAME, ModificarTiempoMinimoYsancionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDatosConductor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerDatosConductor")
    public JAXBElement<ObtenerDatosConductor> createObtenerDatosConductor(ObtenerDatosConductor value) {
        return new JAXBElement<ObtenerDatosConductor>(_ObtenerDatosConductor_QNAME, ObtenerDatosConductor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDatosConductorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerDatosConductorResponse")
    public JAXBElement<ObtenerDatosConductorResponse> createObtenerDatosConductorResponse(ObtenerDatosConductorResponse value) {
        return new JAXBElement<ObtenerDatosConductorResponse>(_ObtenerDatosConductorResponse_QNAME, ObtenerDatosConductorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDatosMovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerDatosMovil")
    public JAXBElement<ObtenerDatosMovil> createObtenerDatosMovil(ObtenerDatosMovil value) {
        return new JAXBElement<ObtenerDatosMovil>(_ObtenerDatosMovil_QNAME, ObtenerDatosMovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerDatosMovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerDatosMovilResponse")
    public JAXBElement<ObtenerDatosMovilResponse> createObtenerDatosMovilResponse(ObtenerDatosMovilResponse value) {
        return new JAXBElement<ObtenerDatosMovilResponse>(_ObtenerDatosMovilResponse_QNAME, ObtenerDatosMovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaMovilesConductoresWS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaMovilesConductoresWS")
    public JAXBElement<ObtenerListaMovilesConductoresWS> createObtenerListaMovilesConductoresWS(ObtenerListaMovilesConductoresWS value) {
        return new JAXBElement<ObtenerListaMovilesConductoresWS>(_ObtenerListaMovilesConductoresWS_QNAME, ObtenerListaMovilesConductoresWS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaMovilesConductoresWSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaMovilesConductoresWSResponse")
    public JAXBElement<ObtenerListaMovilesConductoresWSResponse> createObtenerListaMovilesConductoresWSResponse(ObtenerListaMovilesConductoresWSResponse value) {
        return new JAXBElement<ObtenerListaMovilesConductoresWSResponse>(_ObtenerListaMovilesConductoresWSResponse_QNAME, ObtenerListaMovilesConductoresWSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosCompleta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosCompleta")
    public JAXBElement<ObtenerListaServiciosCompleta> createObtenerListaServiciosCompleta(ObtenerListaServiciosCompleta value) {
        return new JAXBElement<ObtenerListaServiciosCompleta>(_ObtenerListaServiciosCompleta_QNAME, ObtenerListaServiciosCompleta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosCompletaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosCompletaResponse")
    public JAXBElement<ObtenerListaServiciosCompletaResponse> createObtenerListaServiciosCompletaResponse(ObtenerListaServiciosCompletaResponse value) {
        return new JAXBElement<ObtenerListaServiciosCompletaResponse>(_ObtenerListaServiciosCompletaResponse_QNAME, ObtenerListaServiciosCompletaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosHistoricosCompleta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosHistoricosCompleta")
    public JAXBElement<ObtenerListaServiciosHistoricosCompleta> createObtenerListaServiciosHistoricosCompleta(ObtenerListaServiciosHistoricosCompleta value) {
        return new JAXBElement<ObtenerListaServiciosHistoricosCompleta>(_ObtenerListaServiciosHistoricosCompleta_QNAME, ObtenerListaServiciosHistoricosCompleta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosHistoricosCompletaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosHistoricosCompletaResponse")
    public JAXBElement<ObtenerListaServiciosHistoricosCompletaResponse> createObtenerListaServiciosHistoricosCompletaResponse(ObtenerListaServiciosHistoricosCompletaResponse value) {
        return new JAXBElement<ObtenerListaServiciosHistoricosCompletaResponse>(_ObtenerListaServiciosHistoricosCompletaResponse_QNAME, ObtenerListaServiciosHistoricosCompletaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosHistoricosFecha }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosHistoricosFecha")
    public JAXBElement<ObtenerListaServiciosHistoricosFecha> createObtenerListaServiciosHistoricosFecha(ObtenerListaServiciosHistoricosFecha value) {
        return new JAXBElement<ObtenerListaServiciosHistoricosFecha>(_ObtenerListaServiciosHistoricosFecha_QNAME, ObtenerListaServiciosHistoricosFecha.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosHistoricosFechaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosHistoricosFechaResponse")
    public JAXBElement<ObtenerListaServiciosHistoricosFechaResponse> createObtenerListaServiciosHistoricosFechaResponse(ObtenerListaServiciosHistoricosFechaResponse value) {
        return new JAXBElement<ObtenerListaServiciosHistoricosFechaResponse>(_ObtenerListaServiciosHistoricosFechaResponse_QNAME, ObtenerListaServiciosHistoricosFechaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosMemoria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosMemoria")
    public JAXBElement<ObtenerListaServiciosMemoria> createObtenerListaServiciosMemoria(ObtenerListaServiciosMemoria value) {
        return new JAXBElement<ObtenerListaServiciosMemoria>(_ObtenerListaServiciosMemoria_QNAME, ObtenerListaServiciosMemoria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaServiciosMemoriaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaServiciosMemoriaResponse")
    public JAXBElement<ObtenerListaServiciosMemoriaResponse> createObtenerListaServiciosMemoriaResponse(ObtenerListaServiciosMemoriaResponse value) {
        return new JAXBElement<ObtenerListaServiciosMemoriaResponse>(_ObtenerListaServiciosMemoriaResponse_QNAME, ObtenerListaServiciosMemoriaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaTipoServicio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaTipoServicio")
    public JAXBElement<ObtenerListaTipoServicio> createObtenerListaTipoServicio(ObtenerListaTipoServicio value) {
        return new JAXBElement<ObtenerListaTipoServicio>(_ObtenerListaTipoServicio_QNAME, ObtenerListaTipoServicio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListaTipoServicioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerListaTipoServicioResponse")
    public JAXBElement<ObtenerListaTipoServicioResponse> createObtenerListaTipoServicioResponse(ObtenerListaTipoServicioResponse value) {
        return new JAXBElement<ObtenerListaTipoServicioResponse>(_ObtenerListaTipoServicioResponse_QNAME, ObtenerListaTipoServicioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerServicioXtel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerServicioXtel")
    public JAXBElement<ObtenerServicioXtel> createObtenerServicioXtel(ObtenerServicioXtel value) {
        return new JAXBElement<ObtenerServicioXtel>(_ObtenerServicioXtel_QNAME, ObtenerServicioXtel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerServicioXtelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerServicioXtelResponse")
    public JAXBElement<ObtenerServicioXtelResponse> createObtenerServicioXtelResponse(ObtenerServicioXtelResponse value) {
        return new JAXBElement<ObtenerServicioXtelResponse>(_ObtenerServicioXtelResponse_QNAME, ObtenerServicioXtelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerUsuarioSistema }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerUsuarioSistema")
    public JAXBElement<ObtenerUsuarioSistema> createObtenerUsuarioSistema(ObtenerUsuarioSistema value) {
        return new JAXBElement<ObtenerUsuarioSistema>(_ObtenerUsuarioSistema_QNAME, ObtenerUsuarioSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerUsuarioSistemaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "obtenerUsuarioSistemaResponse")
    public JAXBElement<ObtenerUsuarioSistemaResponse> createObtenerUsuarioSistemaResponse(ObtenerUsuarioSistemaResponse value) {
        return new JAXBElement<ObtenerUsuarioSistemaResponse>(_ObtenerUsuarioSistemaResponse_QNAME, ObtenerUsuarioSistemaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecibirCargaTripulacionAvianca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "recibirCargaTripulacionAvianca")
    public JAXBElement<RecibirCargaTripulacionAvianca> createRecibirCargaTripulacionAvianca(RecibirCargaTripulacionAvianca value) {
        return new JAXBElement<RecibirCargaTripulacionAvianca>(_RecibirCargaTripulacionAvianca_QNAME, RecibirCargaTripulacionAvianca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecibirCargaTripulacionAviancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "recibirCargaTripulacionAviancaResponse")
    public JAXBElement<RecibirCargaTripulacionAviancaResponse> createRecibirCargaTripulacionAviancaResponse(RecibirCargaTripulacionAviancaResponse value) {
        return new JAXBElement<RecibirCargaTripulacionAviancaResponse>(_RecibirCargaTripulacionAviancaResponse_QNAME, RecibirCargaTripulacionAviancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecibirDatosAvianca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "recibirDatosAvianca")
    public JAXBElement<RecibirDatosAvianca> createRecibirDatosAvianca(RecibirDatosAvianca value) {
        return new JAXBElement<RecibirDatosAvianca>(_RecibirDatosAvianca_QNAME, RecibirDatosAvianca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecibirDatosAviancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "recibirDatosAviancaResponse")
    public JAXBElement<RecibirDatosAviancaResponse> createRecibirDatosAviancaResponse(RecibirDatosAviancaResponse value) {
        return new JAXBElement<RecibirDatosAviancaResponse>(_RecibirDatosAviancaResponse_QNAME, RecibirDatosAviancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarVersionActual }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "verificarVersionActual")
    public JAXBElement<VerificarVersionActual> createVerificarVersionActual(VerificarVersionActual value) {
        return new JAXBElement<VerificarVersionActual>(_VerificarVersionActual_QNAME, VerificarVersionActual.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarVersionActualResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "verificarVersionActualResponse")
    public JAXBElement<VerificarVersionActualResponse> createVerificarVersionActualResponse(VerificarVersionActualResponse value) {
        return new JAXBElement<VerificarVersionActualResponse>(_VerificarVersionActualResponse_QNAME, VerificarVersionActualResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebLogInUsuarioSistema }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "webLogInUsuarioSistema")
    public JAXBElement<WebLogInUsuarioSistema> createWebLogInUsuarioSistema(WebLogInUsuarioSistema value) {
        return new JAXBElement<WebLogInUsuarioSistema>(_WebLogInUsuarioSistema_QNAME, WebLogInUsuarioSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebLogInUsuarioSistemaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fastcar.rwtec.com/", name = "webLogInUsuarioSistemaResponse")
    public JAXBElement<WebLogInUsuarioSistemaResponse> createWebLogInUsuarioSistemaResponse(WebLogInUsuarioSistemaResponse value) {
        return new JAXBElement<WebLogInUsuarioSistemaResponse>(_WebLogInUsuarioSistemaResponse_QNAME, WebLogInUsuarioSistemaResponse.class, null, value);
    }

}
