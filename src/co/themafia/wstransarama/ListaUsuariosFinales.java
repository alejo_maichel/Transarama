
package co.themafia.wstransarama;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para listaUsuariosFinales complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="listaUsuariosFinales"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tipoProyecto" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listaUsuariosFinales", propOrder = {
    "tipoProyecto"
})
public class ListaUsuariosFinales {

    protected int tipoProyecto;

    /**
     * Obtiene el valor de la propiedad tipoProyecto.
     * 
     */
    public int getTipoProyecto() {
        return tipoProyecto;
    }

    /**
     * Define el valor de la propiedad tipoProyecto.
     * 
     */
    public void setTipoProyecto(int value) {
        this.tipoProyecto = value;
    }

}
