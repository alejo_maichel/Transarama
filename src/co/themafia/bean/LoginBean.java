package co.themafia.bean;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import co.themafia.dto.DatosUsuariosDTO;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion_Service;

@ManagedBean
@SessionScoped
public class LoginBean {

	private boolean logeado;
	private DatosUsuariosDTO usuario = new DatosUsuariosDTO() ;
	private Gson gson = new Gson();

	public boolean isLogeado() {
		return logeado;
	}

	public void setLogeado(boolean logeado) {
		this.logeado = logeado;
	}

	public DatosUsuariosDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(DatosUsuariosDTO usuario) {
		this.usuario = usuario;
	}

	public void realizarLogin() {
		System.out.println("==============================");
		if (usuario != null && !usuario.getNombreUsuarioSistema().isEmpty()) {
			if (usuario != null && !usuario.getClave().isEmpty()) {
				try {
					usuario.setTipoProyecto("5");
					String usuarioJson =  gson.toJson(usuario);
					WSFuncionesSistemaAsignacion_Service service1 = new WSFuncionesSistemaAsignacion_Service();
					WSFuncionesSistemaAsignacion port1 = service1.getWSFuncionesSistemaAsignacionPort();
					String resultado = port1.webLogInUsuarioSistema(usuarioJson);
					
					usuario = gson.fromJson(resultado, DatosUsuariosDTO.class);
					
					if (usuario != null && usuario.getError().equals("")) {
						logeado = true;
						try {
							FacesContext.getCurrentInstance().getExternalContext().redirect("pagesUsuario/Inicio.xhtml");
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR", "No se ha podido autenticar en el sistema"));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}				
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "La contrasena no puede ser vacia"));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "El usuario no puede ser vacio"));
		}
	}

	public void realizarLogout() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.invalidate();
		logeado = false;
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("../Login.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
