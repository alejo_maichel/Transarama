package co.themafia.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.google.gson.Gson;

import au.com.bytecode.opencsv.CSVReader;
import co.themafia.dto.DatosServiciosDTO;
import co.themafia.dto.DatosTransaramaAviancaDTO;
import co.themafia.dto.DatosTransaramaOperativaDTO;
import co.themafia.dto.ErrorDto;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion_Service;

@ManagedBean
@ViewScoped
public class ServiciosBean {

	private Date fechaInicioReporte;
	private Date fechaFinalReporte;
	private List<DatosServiciosDTO> listaServicios;
	private List<DatosServiciosDTO> listaServiciosHistorico;
	private Gson gson = new Gson();
	private WSFuncionesSistemaAsignacion_Service service1;
	private WSFuncionesSistemaAsignacion port1;

	public Date getFechaInicioReporte() {
		return fechaInicioReporte;
	}

	public void setFechaInicioReporte(Date fechaInicioReporte) {
		this.fechaInicioReporte = fechaInicioReporte;
	}

	public Date getFechaFinalReporte() {
		return fechaFinalReporte;
	}

	public void setFechaFinalReporte(Date fechaFinalReporte) {
		this.fechaFinalReporte = fechaFinalReporte;
	}

	public List<DatosServiciosDTO> getListaServicios() {
		return listaServicios;
	}

	public void setListaServicios(List<DatosServiciosDTO> listaServicios) {
		this.listaServicios = listaServicios;
	}

	public List<DatosServiciosDTO> getListaServiciosHistorico() {
		return listaServiciosHistorico;
	}

	public void setListaServiciosHistorico(List<DatosServiciosDTO> listaServiciosHistorico) {
		this.listaServiciosHistorico = listaServiciosHistorico;
	}

	@PostConstruct
	public void init() {
		service1 = new WSFuncionesSistemaAsignacion_Service();
		port1 = service1.getWSFuncionesSistemaAsignacionPort();
	}

	public void listarServiciosActivos() {
		String resultado = port1.obtenerListaServiciosMemoria(5);
		DatosServiciosDTO datosServicios = gson.fromJson(resultado, DatosServiciosDTO.class);
		listaServicios = datosServicios.getListaServicios();
	}

	public void listarServiciosHistorico() {
		String resultado = port1.obtenerListaServiciosHistoricosCompleta(5);
		DatosServiciosDTO datosServicios = gson.fromJson(resultado, DatosServiciosDTO.class);
		listaServiciosHistorico = datosServicios.getListaServicios();
	}

	public void buscarListarServiciosHistorico() {
		DatosServiciosDTO dto = new DatosServiciosDTO();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		dto.setFechaInicialReporte(format.format(fechaInicioReporte));
		dto.setFechaFinalReporte(format.format(fechaFinalReporte));
		String tram = gson.toJson(dto);
		String resultado = port1.obtenerListaServiciosHistoricosFecha(5, tram);
		DatosServiciosDTO datosServicios = gson.fromJson(resultado, DatosServiciosDTO.class);
		listaServiciosHistorico = datosServicios.getListaServicios();
	}

	public void cargarServiciosCsv(FileUploadEvent event) {

		try {
			int cantidadExitosos = 0;
			int cantidadErrores = 0;

			UploadedFile archivoCargado = event.getFile();
			InputStream in = archivoCargado.getInputstream();
			CSVReader reader = new CSVReader(new InputStreamReader(in), ';');
			List<String[]> listaTemporar = reader.readAll();
			for (String[] nextLine : listaTemporar) {

				DatosTransaramaAviancaDTO dto = new DatosTransaramaAviancaDTO();
				System.out.println(nextLine[0]);

				dto.setCedula(nextLine[0]);
				dto.setNombre(nextLine[1]);
				dto.setFecha(nextLine[2]);
				dto.setCargo(nextLine[3]);
				dto.setOrigen(nextLine[4]);
				dto.setVuelo(nextLine[5]);
				dto.setMaquina(nextLine[6]);
				dto.setDestino(nextLine[7]);
				dto.setHora(nextLine[8]);

				String request = gson.toJson(dto);
				String response = port1.recibirDatosAvianca(request);

				ErrorDto respuesta = gson.fromJson(response, ErrorDto.class);
				if (respuesta != null && respuesta.getError().equals("")) {
					cantidadExitosos++;
				} else {
					cantidadErrores++;
				}
			}
			if (cantidadExitosos > 0) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"INFORMACION", "Se han cargado " + cantidadExitosos + " servicios sin problemas"));
			}
			if (cantidadErrores > 0) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"ADVERTENCIA",
						"No se han podido cargar " + cantidadExitosos + " servicios, por favor verifique el archivo"));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cargarTripulacionCsv(FileUploadEvent event) {

		try {
			int cantidadExitosos = 0;
			int cantidadErrores = 0;

			UploadedFile archivoCargado = event.getFile();
			InputStream in = archivoCargado.getInputstream();
			CSVReader reader = new CSVReader(new InputStreamReader(in), ';');
			List<String[]> listaTemporar = reader.readAll();
			// CEDULA;NOMBRE;CARGO;BASE;DIRECCION;DATOS
			// COMPLEMENTARIOS;BARRIO;CELULAR;EMAIL;LATITUD;LONGITUD
			for (String[] nextLine : listaTemporar) {
				DatosTransaramaOperativaDTO dto = new DatosTransaramaOperativaDTO();
				System.out.println(nextLine[0]);

				dto.setDocumento(nextLine[0]);
				dto.setNombre(nextLine[1]);
				dto.setCargo(nextLine[2]);
				dto.setBase(nextLine[3]);
				dto.setDireccion(nextLine[4]);
				dto.setDireccionComplementarios(nextLine[5]);
				dto.setBarrio(nextLine[6]);
				dto.setNumeroCelular(nextLine[7]);
				dto.setCorreoElectronico(nextLine[8]);
				dto.setLatitud(nextLine[9]);
				dto.setLongitud(nextLine[10]);

				String request = gson.toJson(dto);
				String response = port1.recibirCargaTripulacionAvianca(request);

				ErrorDto respuesta = gson.fromJson(response, ErrorDto.class);
				if (respuesta != null && respuesta.getError().equals("")) {
					cantidadExitosos++;
				} else {
					cantidadErrores++;
				}
			}
			if (cantidadExitosos > 0) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"INFORMACION", "Se han cargado " + cantidadExitosos + " tripulantes sin problemas"));
			}
			if (cantidadErrores > 0) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "ADVERTENCIA", "No se han podido cargar "
								+ cantidadExitosos + " tripulantes, por favor verifique el archivo"));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
