package co.themafia.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.google.gson.Gson;

import co.themafia.dto.DatosDireccionesDTO;
import co.themafia.dto.DatosUsuariosDTO;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion;
import co.themafia.wstransarama.WSFuncionesSistemaAsignacion_Service;

@ManagedBean
@ViewScoped
public class UsuarioBean {

	private List<DatosUsuariosDTO> listaUsuarios;
	private Gson gson = new Gson();
	private WSFuncionesSistemaAsignacion_Service service1;
	private WSFuncionesSistemaAsignacion port1;
	private DatosUsuariosDTO usuarioActivo = new DatosUsuariosDTO();
	private DatosDireccionesDTO direccionActivo = new DatosDireccionesDTO();

	public DatosDireccionesDTO getDireccionActivo() {
		return direccionActivo;
	}

	public void setDireccionActivo(DatosDireccionesDTO direccionActivo) {
		this.direccionActivo = direccionActivo;
	}

	public DatosUsuariosDTO getUsuarioActivo() {
		return usuarioActivo;
	}

	public void setUsuarioActivo(DatosUsuariosDTO usuarioActivo) {
		this.usuarioActivo = usuarioActivo;
	}

	public List<DatosUsuariosDTO> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<DatosUsuariosDTO> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	@PostConstruct
	public void init() {
		service1 = new WSFuncionesSistemaAsignacion_Service();
		port1 = service1.getWSFuncionesSistemaAsignacionPort();
	}

	public void listarUsuarios() {
		String respuesta = port1.listaUsuariosFinales(5);
		DatosUsuariosDTO dto = gson.fromJson(respuesta, DatosUsuariosDTO.class);
		listaUsuarios = dto.getListaUsuarios();
	}

	public void agregarNuevoUsuario() {

		usuarioActivo.setTipoProyecto("5");
		String request = gson.toJson(usuarioActivo);
		String response = port1.actualizarModificarUsuario(request);
		DatosUsuariosDTO dto = gson.fromJson(response, DatosUsuariosDTO.class);
		if (dto != null) {
			if (dto.getError().length() > 0) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", dto.getError()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha creado el usuario correctamente"));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "El objeto de respuesta esta nulo"));
		}
		usuarioActivo = new DatosUsuariosDTO();
		listarUsuarios();
	}

	public void modificarUsuario() {
		String request = gson.toJson(usuarioActivo);
		String response = port1.actualizarModificarUsuario(request);
		DatosUsuariosDTO dto = gson.fromJson(response, DatosUsuariosDTO.class);
		if (dto != null) {
			if (dto.getError().length() > 0) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", dto.getError()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO",
						"Se ha modificado el usuario correctamente"));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "El objeto de respuesta esta nulo"));
		}
		usuarioActivo = new DatosUsuariosDTO();
		listarUsuarios();
	}

	public void agregarNuevaDireccion() {
		direccionActivo.setIdUsuario(usuarioActivo.getIdUsuario());
		String request = gson.toJson(direccionActivo);
		boolean response = port1.actualizarDireccionUsuario(request);
		if (response) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha creado la direccion correctamente"));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "No se pudo agregar la direccion"));
		}
		direccionActivo = new DatosDireccionesDTO();
		listarUsuarios();
	}

	public void modificarDireccion() {
		String request = gson.toJson(direccionActivo);
		boolean response = port1.actualizarDireccionUsuario(request);
		if (response) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha creado la direccion correctamente"));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "No se pudo agregar la direccion"));
		}
		direccionActivo = new DatosDireccionesDTO();
		listarUsuarios();

	}

	public void eliminarDireccion(DatosDireccionesDTO direccion) {
		String request = gson.toJson(direccion);
		boolean response = port1.eliminarDireccionUsuario(request);
		if (response) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha eliminado la direccion correctamente"));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "No se pudo eliminar la direccion"));
		}
		direccionActivo = new DatosDireccionesDTO();
		listarUsuarios();
	}

	public void cargarUsuario(DatosUsuariosDTO usuario) {
		usuarioActivo = usuario;
	}

	public void cargarDireccion(DatosDireccionesDTO direccion) {
		direccionActivo = direccion;
	}

}
