package co.themafia.dto;

import java.io.Serializable;

public class ErrorDto implements Serializable{

	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
