package co.themafia.dto;

import java.io.Serializable;

public class DatosConductorDTO implements Serializable{
	
	private String idConductor;
	private String identificacion;
	private String nombres;
	private String apellidos;
	private String telContacto;
	private String telFijo;
	private String clave;
	private String correoElectronico;
	private String disponible;
	private String foto;
	private boolean onLine;
	private String plcaAsignada;
	private String licencia;
	private String fechaVencLicencia;
	private String fechaVencSS;
	// private ArrayList<DatosMovilesDTO> listaMov;
	private String error;
	private String tipoProyecto;

	public String getIdConductor() {
		return idConductor;
	}

	public void setIdConductor(String idConductor) {
		this.idConductor = idConductor;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getTelContacto() {
		return telContacto;
	}

	public void setTelContacto(String telContacto) {
		this.telContacto = telContacto;
	}

	public String getTelFijo() {
		return telFijo;
	}

	public void setTelFijo(String telFijo) {
		this.telFijo = telFijo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDisponible() {
		return disponible;
	}

	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public boolean isOnLine() {
		return onLine;
	}

	public void setOnLine(boolean onLine) {
		this.onLine = onLine;
	}

	public String getPlcaAsignada() {
		return plcaAsignada;
	}

	public void setPlcaAsignada(String plcaAsignada) {
		this.plcaAsignada = plcaAsignada;
	}

	public String getLicencia() {
		return licencia;
	}

	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	public String getFechaVencLicencia() {
		return fechaVencLicencia;
	}

	public void setFechaVencLicencia(String fechaVencLicencia) {
		this.fechaVencLicencia = fechaVencLicencia;
	}

	public String getFechaVencSS() {
		return fechaVencSS;
	}

	public void setFechaVencSS(String fechaVencSS) {
		this.fechaVencSS = fechaVencSS;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getTipoProyecto() {
		return tipoProyecto;
	}

	public void setTipoProyecto(String tipoProyecto) {
		this.tipoProyecto = tipoProyecto;
	}

}
