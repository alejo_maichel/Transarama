package co.themafia.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class DatosTransaramaAviancaDTO implements Serializable {

	private String cedula;
	private String nombre;
	private String fecha;
	private String cargo;
	private String origen;
	private String vuelo;
	private String maquina;
	private String destino;
	private String hora;
	private String error;
	private ArrayList<DatosTransaramaAviancaDTO> solicitudesServicio;

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getVuelo() {
		return vuelo;
	}

	public void setVuelo(String vuelo) {
		this.vuelo = vuelo;
	}

	public String getMaquina() {
		return maquina;
	}

	public void setMaquina(String maquina) {
		this.maquina = maquina;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<DatosTransaramaAviancaDTO> getSolicitudesServicio() {
		return solicitudesServicio;
	}

	public void setSolicitudesServicio(ArrayList<DatosTransaramaAviancaDTO> solicitudesServicio) {
		this.solicitudesServicio = solicitudesServicio;
	}
}
